import React from 'react';
import { Container, Content } from './styles';
import logo from '~/assets/logo.png';

export default function AppFooter() {
  return (
    <Container className="clear">
      <Content>
        <a href="/">
          <img alt="logo-colored" src={logo} />
        </a>
        <span>
          <span className="material-icons">copyright</span>
          <span>Todos os direitos reservados</span>
        </span>
      </Content>
    </Container>
  );
}
