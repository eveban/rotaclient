import styled from 'styled-components';

export const Container = styled.div`
  background: #fff;
  padding: 0 30px;
  position: fixed;
  bottom: 0px;
  left: 0px;
  right: 0px;
  overflow: hidden;
`;
export const Content = styled.div`
  height: 64px;
  max-width: 90%;
  margin: 0px auto;
  display: flex;
  justify-content: space-between;
  align-items: center;

  img {
    margin-top: 5px;
    width: 100px;
  }
  .clearfix:after {
    content: ' ';
    display: block;
    clear: both;
  }
  span {
    float: right;
    margin-top: 10px;
  }
`;
