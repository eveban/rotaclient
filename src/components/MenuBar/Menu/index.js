import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppSubmenu from '../AppSubmenu';

export class AppMenu extends Component {
  static defaultProps = {
    model: null,
    onRootMenuItemClick: null,
    onMenuItemClick: null,
    layoutMode: null,
    active: false,
  };

  static propTypes = {
    model: PropTypes.array,
    layoutMode: PropTypes.string,
    onRootMenuItemClick: PropTypes.func,
    onMenuItemClick: PropTypes.func,
    active: PropTypes.bool,
  };

  render() {
    return (
      <AppSubmenu
        items={this.props.model}
        className="layout-menu"
        menuActive={this.props.active}
        onMenuItemClick={this.props.onMenuItemClick}
        onRootItemClick={this.props.onRootMenuItemClick}
        root
        layoutMode={this.props.layoutMode}
        parentMenuItemActive
      />
    );
  }
}
