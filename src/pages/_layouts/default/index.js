import React from 'react';
import PropTypes from 'prop-types';
import { Wrapper } from './styles';
import Header from '~/components/Header';
import Menu from '~/components/MenuBar';
import Footer from '~/components/Footer';

export default function DefaultLayout({ children }) {
  return (
    <Wrapper>
      <Header />
      <Menu />
      {children}
      <Footer />
    </Wrapper>
  );
}
DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
